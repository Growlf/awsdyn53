# AWSDyn53 Version 1.0 #

AWSDyn53 Requires [Boto](https://pypi.python.org/pypi/boto), but no other non-standard libraries.  To use it, simply download the script and run it.  To get help and usage, use the standard '-h' flag in the commandline.

### How do I get set up? ###

* Install [Boto](https://pypi.python.org/pypi/boto). Typically, on a linux box: ```sudo pip install boto```
* Download the [update.py](https://bitbucket.org/Growlf/awsdyn53/raw/master/update.py) file (or use git to download the repository - git clone https://bitbucket.org/Growlf/awsdyn53.git) and run it.

Optionally, you may want to set up a cron job or similar to keep your DNS updated by running the command regularly.

### Need more info? ###

* See the [Wiki](awsdyn53/wiki)
* Message me

### Whats next? ###

**Packaging for PyPi**. The next version of this app is expected to be installable from PyPi and have command code which will be added directly to the local system's Python binary directory (or virtual environment) for easier use.