#!/usr/bin/env python
__author__ = 'growlf@oddotter.com'

"""
This simple script will attempt to determine your IP address and update an
Amazon Route 53 account with it.  Make sure to set your domain, system name
and AWS login info in your shell python before running this, like so:

export AWS_ACCESS_KEY_ID="<Insert your AWS Access Key>"
export AWS_SECRET_ACCESS_KEY="<Insert your AWS Secret Key>"
export AWS_R53_ZONE="<Insert your Route53 enabled domain name>"
export HOSTNAME="<Insert the systen name to be added to the domain>"

If the HOSTNAME is not specified (either by environment or command parameter),
your system's hostname will be used instead.

Optionally, you can specify each of these with commandline parameters instead
which will also override any environmental settings.

Once you have the utility running as you would like, you can add a crontab line 
like the following to make the updates automatic:

*  *  *  *  *   /home/ubuntu/bin/dyndnsupdate -z <DNS zone> -n <host name>

You can test the command like so (from the commandline):


If you want more information on how to get your external IP address, please 
see the following page:

https://www.ipify.org/

"""

import os
import logging
import socket
import urllib2
import boto
from boto.route53.record import ResourceRecordSets


logging.basicConfig(level=logging.CRITICAL)
logger = logging.getLogger(__name__)


def get_my_ip(rdns):
    """
    Quick and simple method of getting your "real" IP from an external source.

    The rdns can be any web page that returns JUST the IP address of the client
    browser.  Feel free to create your own in PHP, Django, Flask, or what have you.
    For simplicity's sake, we just use an existing and popular one from the command
    default.

    On the off chance that you have used your own or the the default one that we
    use does not gives us a pure text-only response with a valid IP, we do a little
    checking.
    """

    # Get the external IP address from an "outside reflection" source
    rq = urllib2.urlopen(rdns)

    # Strip off any excess characters as we just want a bare IP address
    ip = rq.readline().strip()

    # Split the response into what should be 'octets'
    octets = ip.split('.')

    # Make sure we actually received all 4 octets in the response
    if len(octets) != 4:
        logger.critical(
            "Error! A useful IP was not returned from %s. Address value was malformed or not actually an address." %
            rdns)
        return False

    # Lets test the returned IP for validity before doing anything with it
    for ot in octets:
        try:
            # As we loop through the octets, lets make sure they are integers
            ot_i = int(ot)
        except ValueError as e:
            logger.critical("Error! Invalid IP returned.  The octets contain non-integer data.")
            return False
        else:
            # And since they are integers, are they in the proper range?
            if ot_i < 1 or ot_i > 254:
                logger.critical("Error! Invalid IP returned.  The octets contain invalid values.")
                return False
    return ip


def update_dns(zonename, hostname, ip, ttl):
    """
    Connects to the AWS account specified and updates the specified DNS zone with host information
    """

    # Connect up to the AWS account
    r53 = boto.connect_route53()

    # Get the Zone ID for the target domain
    zone = r53.get_hosted_zone_by_name(zonename)
    if not zone:
        logger.critical("Error! The host zone '%s' does not exist in the AWS account. The host/system '%s' cannot be "
                        "added nor created within it.  Please go to https://console.aws.amazon.com/route53/ and "
                        "create the expected zone or correct the parameters for this command."
                        % (zonename, hostname))
        return False

        ###TODO: maybe add zone autocreate code here.  We have the access, why not just auto-make the zone too?
        ### ...other than people will start making example.com entries in their Route53 by default...

    # Parse off the unwanted data - it comes over looking like this:
    #   {'Id':'/hostedzone/Z2ZMWUDG6PRYT6'}  - and we only want the Z2ZMWUDG6PRYT6 part
    zone_id = zone['GetHostedZoneResponse']['HostedZone']['Id'].split('/')[-1:][0]

    # Create an FQDN for the target host
    fqdn_hostname = hostname + "." + zonename + "."

    # UPdate or inSERT the record (this is new option in the BOTO API)
    changes = ResourceRecordSets(r53, zone_id)
    change = changes.add_change("UPSERT", fqdn_hostname, 'A', ttl)
    change.add_value(ip)
    change_status = changes.commit()['ChangeResourceRecordSetsResponse']['ChangeInfo']

    logger.info("'%s' was added to the '%s' zone at '%s' and should be immediately available." %
                (hostname, zonename, ip))

    return change_status


# If this is used as a commandline utility instead of a library:
if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        description="Update an AWS Route 53 zone with this system's current IP and host name.",
        epilog="Before using this command, ensure that you have first created the zone in your AWS Route53 account."
               "Please go to https://console.aws.amazon.com/route53/ and create the expected zone or correct the "
               "parameters for this command. "
               "Additionally, you can set all of these parameters in your environment by simply adding lines like the "
               "following to your .bashrc (for instance): 'export AWS_R53_ZONE=mydomain.com'"
    )

    # Get all of our parameters and defaults.  Commandline options override environmental ones.
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    parser.add_argument('--verbose', '-v', action='count',
                        help='Increase the verbosity of the output - default is none')
    parser.add_argument('-z', '--aws_r53_zone', default=os.environ.get('AWS_R53_ZONE', 'example.com'),
                        help='DNS zone to add the hostname to')
    parser.add_argument('-l', '--dns_ttl', default=os.environ.get('DNS_TTL', 60),
                        help='Time to live before refreshing for this record')
    parser.add_argument('-n', '--hostname', default=os.environ.get('HOSTNAME', socket.gethostname()),
                        help='Hostname to be added to the zone. Default is this system\'s hostname.')
    parser.add_argument('-w', '--myip_webserver', default=os.environ.get('MYIP_WEBSERVER', "https://api.ipify.org/"),
                        help='web server to receive the "real" or reflected IP of this system from')
    parser.add_argument('-i', '--aws_access_key_id', default=os.environ.get('AWS_ACCESS_KEY_ID'),
                        help='your AWS account access key ID - may be an IAM key')
    parser.add_argument('-k', '--aws_secret_access_key', default=os.environ.get('AWS_SECRET_ACCESS_KEY'),
                        help='your AWS secret key - may be an IAM secret key')
    args = parser.parse_args()

    if args.verbose == 1:
        logging.basicConfig(level=logging.WARNING)
    elif args.verbose >= 2:
        logging.basicConfig(level=logging.INFO)

    # Call the update in AWS
    update_dns(args.aws_r53_zone, args.hostname, get_my_ip(args.myip_webserver), args.dns_ttl)
